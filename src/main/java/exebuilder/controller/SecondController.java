package exebuilder.controller;

import exebuilder.core.Controllers;
import exebuilder.core.ViewLoader;
import exebuilder.core.Windows;
import exebuilder.data.Description;
import exebuilder.ui.ExeBuilderWindow;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.TextField;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

public class SecondController implements Initializable {

 private static final Logger logger = LogManager.getLogger();
 public TextField sourcePathField;
 public TextField jdkPathField;
 public TextField mainJarFileField;
 public TextField mainClassField;
 public TextField libPathField;
 public ComboBox<Integer> jdkVersionComboBox;
 public Hyperlink downloadJdkLink;


 public void chooseJdkPath(ActionEvent actionEvent) {
  var directoryChooser = new DirectoryChooser();
  var path = directoryChooser.showDialog(null);
  if (path != null) {
   String jdkPath = path.getAbsolutePath();
   jdkPathField.setText(jdkPath);
   logger.debug("jdk path：{}", jdkPath);
  }
 }

 public void chooseSourcePath(ActionEvent actionEvent) {
  var directoryChooser = new DirectoryChooser();
  var path = directoryChooser.showDialog(null);
  if (path != null) {
   String sourcePath = path.getAbsolutePath();
   sourcePathField.setText(sourcePath);
   logger.debug("source path：{}", sourcePath);
  }
 }

 public void chooseMainJarFile(ActionEvent actionEvent) {
  var fileChooser = new FileChooser();
  fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("可执行的jar文件", "*.jar"));
  if (StringUtils.isNotBlank(sourcePathField.getText())) {
   fileChooser.setInitialDirectory(new File(sourcePathField.getText()));
  }
  var file = fileChooser.showOpenDialog(null);
  if (file != null) {
   var fileName = file.getName();
   mainJarFileField.setText(fileName);
   logger.debug("mainJar file：{}", fileName);
  }
 }

 public void chooselibPath(ActionEvent actionEvent) {
  var directoryChooser = new DirectoryChooser();
  if (StringUtils.isNotBlank(sourcePathField.getText())) {
   directoryChooser.setInitialDirectory(new File(sourcePathField.getText()));
  }
  var path = directoryChooser.showDialog(null);
  if (path != null) {
   var libPath = path.getAbsolutePath();
   libPathField.setText(libPath);
   logger.debug("lib path：{}", libPath);
  }
 }

 public void openDownloadJdkWin() {
  var downloadJdkWindow = new ExeBuilderWindow("downloadJdk");
  var root = ViewLoader.load("downloadJdkView");
  var rootScene = new Scene(root);
  downloadJdkWindow.setScene(rootScene);
  downloadJdkWindow.setResizable(false);
  downloadJdkWindow.setTitle(Description.DOWNLOAD_JDK);
  downloadJdkWindow.initOwner(Windows.get("main"));
  downloadJdkWindow.initModality(Modality.APPLICATION_MODAL);
  downloadJdkWindow.show();

 }


 @Override
 public void initialize(URL url, ResourceBundle resourceBundle) {
  Controllers.add("second", this);
  ObservableList<Integer> jdkVersions = FXCollections.observableArrayList(17);
  jdkVersionComboBox.setItems(jdkVersions);
  jdkVersionComboBox.setValue(17);
 }
}
