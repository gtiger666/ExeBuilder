package exebuilder.data;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class JdkVenders {

 private static final Map<String, String> jdks = new HashMap<>();

 static {
  jdks.put("jdk-17_windows-x64_bin.zip",
    "https://download.oracle.com/java/17/latest/jdk-17_windows-x64_bin.zip");
  jdks.put("zulu17.28.13-ca-jdk17.0.0-win_x64.zip", "https://cdn.azul.com/zulu/bin/zulu17.28.13-ca-jdk17.0.0-win_x64.zip");
 }


 public static String[] names() {
  String[] names = jdks.keySet().toArray(new String[0]);
  Arrays.sort(names);
  return names;
 }

 public static String getUrl(String name) {
  return jdks.get(name);
 }

 public static String getDefault() {
  return names()[0];
 }
}
