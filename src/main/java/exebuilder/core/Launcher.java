package exebuilder.core;

import exebuilder.data.AppConfig;
import javafx.application.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Launcher {

 private static final Logger logger = LogManager.getLogger();

 public static void main(String[] args) {
  System.setProperty("https.protocols", "TLSv1");
  var tempDir = System.getProperty("java.io.tmpdir");
  var appConfig = AppConfig.getInstance();
  appConfig.setTempDir(tempDir);
  logger.debug("应用程序临时目录：{}", tempDir);
  logger.debug("应用程序当前路径：{}", System.getProperty("user.dir"));
  Application.launch(AppMain.class, args);
 }

}
