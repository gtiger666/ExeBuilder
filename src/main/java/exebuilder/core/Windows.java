package exebuilder.core;

import exebuilder.ui.ExeBuilderWindow;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

public class Windows {

  private static final Logger logger = LogManager.getLogger();

  public static final Map<String, ExeBuilderWindow> windows = new HashMap<>();

  public static void add(String key, ExeBuilderWindow window) {
    windows.put(key, window);
    logger.debug("window {} added", key);
  }

  public static ExeBuilderWindow get(String key) {
    return windows.get(key);
  }

  public static void remove(String key) {
    windows.remove(key);
    logger.debug("window {} removed", key);
  }
}
